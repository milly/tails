# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-11-18 12:14+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Call for testing: 3.0~alpha1\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"2016-11-18 11:00:00\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr ""

#. type: Plain text
msgid ""
"You can help Tails! The first alpha for the upcoming version 3.0 is out. We "
"are very excited and cannot wait to hear what you think about it :)"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "What's new in 3.0?"
msgstr ""

#. type: Plain text
msgid ""
"Tails 3.0 will be the first version of Tails based on Debian 9 (Stretch). As "
"such, it upgrades essentially all included software."
msgstr ""

#. type: Plain text
msgid ""
"It also requires a 64-bit computer, and GNOME Shell is now configured to use "
"its default black theme."
msgstr ""

#. type: Plain text
msgid ""
"Technical details of all the changes are listed in the "
"[Changelog](https://git-tails.immerda.ch/tails/plain/debian/changelog?h=feature/stretch)."
msgstr ""

#. type: Title #
#, no-wrap
msgid "How to test Tails 3.0~alpha1?"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"**Keep in mind that this is a test image.** We tested\n"
"that it is not broken in obvious ways, but it might still contain\n"
"undiscovered issues.\n"
msgstr ""

#. type: Plain text
msgid "But test wildly!"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"If you find anything that is not working as it should, please report to\n"
"us <tails-testers@boum.org>.\n"
msgstr ""

#. type: Plain text
#:  
#, no-wrap
msgid ""
"Bonus points if you first check if it is a\n"
"<a href=\"#known_issues\">known issue of this release</a> or a\n"
"[[longstanding known issue|support/known_issues]].\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "Download and install\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<a class=\"download-file use-mirror-pool\" "
"href=\"http://dl.amnesia.boum.org/tails/alpha/tails-amd64-3.0~alpha1/tails-amd64-3.0~alpha1.iso\">Tails "
"3.0~alpha1 ISO image</a>\n"
"<span class=\"openpgp-small-link\">[[OpenPGP "
"signature|torrents/files/tails-amd64-3.0~alpha1.iso.sig]]</span>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"bug\">\n"
msgstr ""

#. type: Plain text
msgid ""
"You cannot install Tails 3.0~alpha1 from Tails 2.x. It is impossible as well "
"to upgrade to Tails 3.0~alpha1 from Tails 2.x. So, either install or upgrade "
"from a non-Tails system, or start Tails 3.0~alpha1 from DVD and then clone "
"it to a USB stick."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr ""

#. type: Plain text
msgid ""
"To install 3.0~alpha1, follow our usual [[installation "
"instructions|install]], skipping the **Download and verify** step."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"If you find anything that is not working as it should, please report to\n"
"us on <tails-testers@boum.org>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"known_issues\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Known issues in 3.0~alpha1\n"
"=========================\n"
msgstr ""

#. type: Bullet: '* '
msgid "The documentation was not adjusted yet."
msgstr ""

#. type: Bullet: '* '
msgid ""
"[Open tickets for Tails "
"3.0](https://labs.riseup.net/code/projects/tails/issues?per_page=100&query_id=198)"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* If you have the GnuPG persistence feature enabled, update files in\n"
"  `/home/amnesia/.gnupg/`:\n"
"  1. Set up an administration password and log in.\n"
"  2. Import `dirmngr.conf` from\n"
"     "
"`/lib/live/mount/rootfs/filesystem.squashfs/etc/skel/.gnupg/dirmngr.conf`.\n"
"  3. Backup the `/home/amnesia/.gnupg/gpg.conf` file, replace it with\n"
"     "
"`/lib/live/mount/rootfs/filesystem.squashfs/etc/skel/.gnupg/gpg.conf`,\n"
"     and re-apply your custom settings on top of the new file.\n"
msgstr ""

#. type: Bullet: '* '
msgid "[[Longstanding known issues|support/known_issues]]"
msgstr ""
