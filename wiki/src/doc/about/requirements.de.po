# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails website\n"
"POT-Creation-Date: 2017-03-20 10:58+0100\n"
"PO-Revision-Date: 2017-04-08 10:25+0100\n"
"Last-Translator: spriver <spriver@autistici.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"System requirements\"]]\n"
msgstr "[[!meta title=\"Systemvoraussetzungen\"]]\n"

#. type: Plain text
msgid ""
"Tails works on most reasonably recent computers, say manufactured after "
"2005.  Here is a detailed list of requirements:"
msgstr ""
"Tails läuft auf den meisten halbwegs aktuellen Computern (d.h. solche, die "
"nach 2005 hergestellt wurden). Folgende Voraussetzungen muss der Rechner "
"jedoch erfüllen:"

#. type: Bullet: '- '
msgid ""
"Either **an internal or external DVD reader** or the possibility to **boot "
"from a USB stick or SD card**."
msgstr ""
"Ein **internes oder externes DVD-Laufwerk**, oder die Möglichkeit von einem "
"**USB-Stick oder einer SD-Karte zu starten**."

#. type: Bullet: '- '
msgid ""
"Tails requires an <span class=\"definition\">[[!wikipedia x86]]</span> "
"compatible processor: **<span class=\"definition\">[[!wikipedia "
"IBM_PC_compatible]]</span>** and others but not <span class=\"definition"
"\">[[!wikipedia PowerPC]]</span> nor <span class=\"definition\">[[!wikipedia "
"ARM]]</span>. Mac computers are IBM PC compatible since 2006."
msgstr ""
"Tails benötigt einen Prozessor, der auf der <span class=\"definition\">[[!"
"wikipedia_de x86 desc=\"x86-Architektur\"]]</span> basiert. Deshalb läuft es "
"auf den meisten gängigen **<span class=\"definition\">[[!wikipedia_de IBM-PC-"
"kompatibler_Computer desc=\"IBM-PC-kompatiblen Computern\"]]</span>** (z. B. "
"Windows-PCs), aber nicht auf <span class=\"definition\">[[!wikipedia_de "
"PowerPC]]</span>- oder <span class=\"definition\">[[!wikipedia_de ARM-"
"Architektur desc=\"ARM\"]]</span>-Rechnern. Mac-Computer sind seit 2006 auch "
"kompatibel zu IBM-PCs."

#. type: Plain text
#, no-wrap
msgid ""
"  **Note**: [[news/Tails 3.0 will require a 64-bit processor]].\n"
"- **2 GB of RAM** to work smoothly. Tails is known to work with less memory\n"
"  but you might experience strange behaviours or crashes.\n"
msgstr ""
" **Hinweis**: [[news/Tails 3.0 wird einen 64-bit Prozessor voraussetzen]].\n"
"- **2GB RAM**, um sauber zu arbeiten. Notfalls läuft es auch mit weniger Arbeitsspeicher,\n"
"allerdings kann es dann zu unerwarteten Störungen oder Systemabstürzen kommen.\n"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"note\">\n"
"See the [[known hardware compatibility issues|support/known_issues]].\n"
"</div>\n"
msgstr ""
"<div class=\"note\">\n"
"Lesen Sie die [[bekannten Probleme mit Hardwarekompatibilität|support/known_issues]].\n"
"</div>\n"
