# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-05-17 09:40+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: ita <transitails@inventati.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails user support mailing list\"]]\n"
msgstr ""

#. type: Plain text
msgid "This list is **the right place** for:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**User support**: solving doubts about how to use Tails, security "
"implications of Tails, etc."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**Community interaction**: information and tricks about ways of using Tails "
"that are not officially documented but might be of interest to others."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**Small issues**: pointing out small things that we needed to fix and do not "
"require a complete bug report.  That's fine and we appreciate it."
msgstr ""

#. type: Plain text
msgid "This list is **the wrong place** for:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**Bug reports**: see the [[bug reporting documentation|/doc/first_steps/"
"bug_reporting]] instead."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**Feature requests**: those should be done on the development mailing list: "
"[[tails-dev@boum.org|about/contact#tails-dev]]."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**Computer security essays and conspiranoia** not directly related to Tails."
msgstr ""

#. type: Plain text
msgid ""
"Public archive of tails-support: <https://mailman.boum.org/pipermail/tails-"
"support/>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<form method=\"POST\" action=\"https://mailman.boum.org/subscribe/tails-support\">\n"
"\t<input class=\"text\" name=\"email\" value=\"\"/>\n"
"\t<input class=\"button\" type=\"submit\" value=\"Subscribe\"/>\n"
"</form>\n"
msgstr ""
